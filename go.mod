module gitlab.com/danabanana/utils

go 1.15

require (
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-openapi/strfmt v0.20.1 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/olivere/elastic v6.2.35+incompatible
	github.com/sirupsen/logrus v1.8.1
)
