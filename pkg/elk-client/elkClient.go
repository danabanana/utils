package elk_client

import (
	"context"
	"github.com/olivere/elastic"
	"github.com/olivere/elastic/config"
)

type ESClient struct {
	client *elastic.Client
}

func Get(config *config.Config) (*ESClient, error) {
	client, err := elastic.NewClientFromConfig(config)
	if err != nil {
		return nil, err
	}
	return &ESClient{ client: client }, nil
}

// method Put - puts data into provided index, and returns its id
func (es *ESClient) Put(ctx context.Context, indexName string, msg interface{}) error {
	_, err := es.client.Index().
		Index(indexName).
		Type("info").
		BodyJson(msg).
		Do(ctx)
	return err
}
