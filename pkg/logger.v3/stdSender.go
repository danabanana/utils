package logger

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"os"
)

type STDSender struct {
	log *logrus.Logger
}

func NewSTDSender() (*STDSender, error) {
	return &STDSender{log: &logrus.Logger{
		Out: os.Stdout,
		Formatter: new(logrus.TextFormatter),
		Hooks: make(logrus.LevelHooks),
		Level: logrus.TraceLevel,
	},
	}, nil
}

func (std *STDSender) Put(msg Log) error {
	messageJson, err := json.Marshal(&msg)
	if err != nil {
		return err
	}

	//message := fmt.Sprintf("service %v: %v, action: %v, message: %v", msg.Service, msg.CreatedAt.Format("2006-01-02 15:04:05"), msg.Action, msg.Message,)
	switch msg.Type {
	case "Trace":
		std.log.Trace(string(messageJson))

	case "Debug":
		std.log.Debug(string(messageJson))

	case "Info":
		std.log.Info(string(messageJson))

	case "Warn":
		std.log.Warn(string(messageJson))

	case "Error", "Fatal":
		std.log.Error(string(messageJson))

	default:
		std.log.Info(string(messageJson))
	}

	return nil
}