package logger

type LogLevel int8

const (
	Fatal LogLevel = 0
	Error = 1
	Warn = 2
	Info = 3
	Debug = 4
	Trace = 5
)

var logLevelToString = map[LogLevel]string {
	Fatal: "Fatal",
	Error: "Error",
	Warn: "Warn",
	Info: "Info",
	Debug: "Debug",
	Trace: "Trace",
}

var stringToLogLevel = map[string]LogLevel {
	"Fatal": Fatal,
	"Error": Error,
	"Warn": Warn,
	"Info": Info,
	"Debug": Debug,
	"Trace": Trace,
}

func (l *LogLevel) String() string {
	return logLevelToString[*l]
}

func (l *LogLevel) ToString(logType LogLevel) string {
	return logLevelToString[logType]
}

func StringToLogLevel(str string) LogLevel {
	return stringToLogLevel[str]
}