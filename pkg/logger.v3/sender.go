package logger

type Sender interface {
	Put(log Log) error
}