package logger

import (
	"encoding/json"
	"fmt"
	httpV2 "gitlab.com/danabanana/utils/pkg/http.v2"
	"gitlab.com/danabanana/utils/pkg/model"
	"net/http"
	"strconv"
	"time"
)

const defaultLocale = "EN"

type Logger struct {
	service  string
	sender   Sender
	logLevel LogLevel
	codes    map[string]model.CodeError
}

func getErrorCodes(codesURL string) (map[string]model.CodeError, error) {
	codes := map[string]model.CodeError{}

	headers := make(map[string]string)
	_, _, err := httpV2.RequestJSON(http.MethodGet, codesURL, nil, headers, &codes)
	if err != nil {
		return codes, err
	}

	return codes, nil
}

func NewLogger(service string, logLevel LogLevel, sender Sender, codesURL string) (*Logger, error) {
	codes, err := getErrorCodes(codesURL)
	if err != nil {
		fmt.Println("Error while getting error codes:", err)
		return nil, err
	}

	return &Logger{
		service:  service,
		sender:   sender,
		logLevel: logLevel,
		codes:    codes,
	}, nil
}

func (log *Logger) log(l *Log) {
	if log.sender == nil {
		fmt.Println("No Sender initialized")
		return
	}
	l.CreatedAt = time.Now()
	if l.Code != 0 {
		codeError, ok := log.codes[strconv.Itoa(l.Code)]
		codeMsg := codeError.Messages[defaultLocale]
		if ok {
			l.CodeMessage = codeMsg
		}
		if len(codeError.Action) > 0 {
			l.Action = codeError.Action
		}
	}
	l.Service = log.service

	err := log.sender.Put(*l)
	if err != nil {
		fmt.Println("Put error:", err.Error())
	}
}

func (log *Logger) Fatal(code int, err error, data ...interface{}) {
	message := ""
	if err != nil {
		message = err.Error()
	}

	if log.logLevel >= Fatal {
		log.log(&Log{
			Action:  "unknown",
			Message: message,
			Code:    code,
			Type:    log.logLevel.ToString(Fatal),
			Data:    ParseData(data),
		})
	}
}

func (log *Logger) FatalResponse(code int, err error, locale string, data ...interface{}) (errorResponse *model.ErrorResponse) {
	errCodes, ok := log.codes[strconv.Itoa(code)]
	errorResponse = &model.ErrorResponse{
		ClientMessage: "Something went wrong!",
		HTTPStatus:    500,
		Code:          999,
	}

	if ok {
		clientMessage := errCodes.Messages[locale]

		if clientMessage == "" {
			clientMessage = errCodes.Messages[defaultLocale]
		}

		// create error response
		errorResponse = &model.ErrorResponse{
			ClientMessage: clientMessage,
			HTTPStatus:    errCodes.HttpCode,
			Code:          code,
		}
	}

	// log error
	if log.logLevel >= Fatal {
		log.Fatal(code, err, data)
	}
	return
}

func (log *Logger) Error(code int, err error, data ...interface{}) {
	message := ""
	if err != nil {
		message = err.Error()
	}

	if log.logLevel >= Error {
		log.log(&Log{
			Action:  "unknown",
			Message: message,
			Code:    code,
			Type:    log.logLevel.ToString(Error),
			Data:    ParseData(data),
		})
	}
}

func (log *Logger) ErrorResponse(code int, err error, locale string, data ...interface{}) (errorResponse *model.ErrorResponse) {
	errCodes, ok := log.codes[strconv.Itoa(code)]
	errorResponse = &model.ErrorResponse{
		ClientMessage: "Something went wrong!",
		HTTPStatus:    500,
		Code:          999,
	}

	if ok {
		clientMessage := errCodes.Messages[locale]

		if clientMessage == "" {
			clientMessage = errCodes.Messages[defaultLocale]
		}

		// create error response
		errorResponse = &model.ErrorResponse{
			ClientMessage: clientMessage,
			HTTPStatus:    errCodes.HttpCode,
			Code:          code,
		}
	}

	// log error
	if log.logLevel >= Error {
		log.Error(code, err, data)
	}
	return
}

func (log *Logger) Warn(action, message string, data ...interface{}) {
	if log.logLevel >= Warn {
		log.log(&Log{
			Action:  action,
			Message: message,
			Type:    log.logLevel.ToString(Warn),
			Data:    ParseData(data),
		})
	}
}

func (log *Logger) Info(action, message string, data ...interface{}) {
	if log.logLevel >= Info {
		log.log(&Log{
			Action:  action,
			Message: message,
			Type:    log.logLevel.ToString(Info),
			Data:    ParseData(data),
		})
	}
}

func (log *Logger) Debug(action, message string, data ...interface{}) {
	if log.logLevel >= Debug {
		log.log(&Log{
			Action:  action,
			Message: message,
			Type:    log.logLevel.ToString(Debug),
			Data:    ParseData(data),
		})
	}
}

func (log *Logger) Trace(action, message string, data ...interface{}) {
	if log.logLevel >= Trace {
		log.log(&Log{
			Action:  action,
			Message: message,
			Type:    log.logLevel.ToString(Trace),
			Data:    ParseData(data),
		})
	}
}

type Log struct {
	CreatedAt   time.Time `json:"createdAt,omitempty"`
	Service     string    `json:"service"`
	Action      string    `json:"action"`
	Code        int       `json:"code,omitempty"`
	CodeMessage string    `json:"codeMessage,omitempty"`
	Message     string    `json:"message,omitempty"`
	Data        string    `json:"data"`
	Type        string    `json:"type"`
}

// ParseData - returns data in string format
func ParseData(data interface{}) string {
	dataByte, err := json.Marshal(data)
	if err != nil && data != nil {
		return ""
	}
	return string(dataByte)
}
