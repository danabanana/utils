package logger

import (
	"context"
	"github.com/olivere/elastic/config"
	elk_client "gitlab.com/danabanana/utils/pkg/elk-client"
)

type ESLoggerConfig struct {
	URL      string
	Username string
	Password string
	Index    string
}

type ESSender struct {
	client    *elk_client.ESClient
	IndexName string
}

func NewESSender(esLoggerConfig ESLoggerConfig) (*ESSender, error) {
	esConfig := &config.Config{
		URL:      esLoggerConfig.URL,
		Username: esLoggerConfig.Username,
		Password: esLoggerConfig.Password,
	}
	client, err := elk_client.Get(esConfig)
	if err != nil {
		return nil, err
	}

	return &ESSender{client: client, IndexName: esLoggerConfig.Index}, nil
}

func (es *ESSender) Put(msg Log) error {
	return es.client.Put(context.Background(), es.IndexName, msg)
}
