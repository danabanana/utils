package model

type CodeError struct {
	Messages 	map[string]string	`json:"messages"`
	HttpCode  	int  	 	 	   	`json:"httpCode"`
	Action		string				`json:"action"`
}
