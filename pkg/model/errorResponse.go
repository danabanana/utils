package model

import (
	"fmt"
)

type ErrorResponse struct {
	error
	ClientMessage string `json:"clientMessage"`
	HTTPStatus    int    `json:"httpStatus"`
	Code          int    `json:"code"`
}

func (err ErrorResponse) Error() string {
	return fmt.Sprintf("%v %v %v", err.HTTPStatus, err.Code, err.ClientMessage)
}

func DefaultErrorResponse() *ErrorResponse {
	return &ErrorResponse{
		ClientMessage: "Something went wrong!",
		HTTPStatus:      500,
		Code:          999,
	}
}
