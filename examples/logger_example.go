package main

import (
	"errors"
	log "gitlab.com/danabanana/utils/pkg/logger.v3"
)

func main() {
	// create logger with ESSender
	sender, err := log.NewESSender(log.ESLoggerConfig{
		URL:      "http://soft.s10s.co:9200",
		Username: "elastic",
		Password: "lmytpL7lIKVwqDGiHWxkd3wmVgAsEvm",
		Index:    "logs",
	})

	if err != nil {
		panic(err)
	}

	logger, err := log.NewLogger("test", log.Trace, sender, "url")
	if err != nil {
		panic(err)
	}

	// create logger with logrus (stdOut)
	//sender, err := log.NewSTDSender()
	//logger, err := log.NewLogger("test", log.Trace, sender)
	//if err != nil {
	//	panic(err)
	//}

	logger.Warn("uiserID", "warn")
	logger.Trace("uiserID", "tracing")
	logger.ErrorResponse(1001, errors.New("sample error"), "RU")
	logger.FatalResponse(4000, errors.New("sample error 2"), "RU")
	logger.Trace("uiserID", "tracing")
}
